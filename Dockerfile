FROM python:3.6.1-slim
MAINTAINER KRATIK JAIN

ENV PYTHONUNBUFFERED 1

#RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./djangoboards /app

#CMD python manage.py migrate
#RUN adduser -D user
#USER user
#
